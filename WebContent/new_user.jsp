<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ page import="java.io.*, java.sql.*, javax.servlet.*, javax.servlet.http.*, java.util.*" %>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="../../assets/ico/favicon.ico">

    <title>New User Page</title>

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="starter-template.css" rel="stylesheet">

    <!-- Just for debugging purposes. Don't actually copy this line! -->
    <!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>

  <body>

    <div class="navbar navbar-inverse navbar-fixed-top" role="navigation" style="background-color:#101010">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="#">Team Pomodori</a>
        </div>
        <div class="collapse navbar-collapse">
          <ul class="nav navbar-nav">
            <li class="active"><a href="overview.jsp">Overview</a></li>
            <li><a href="your_classes.jsp">Classes</a></li>
            <li><a href="your_tasks.jsp">Tasks</a></li>
            <li><a href="timer.jsp">Timer</a>
          </ul>
          <form class="navbar-form navbar-right" role="form" action="/WebJamProject/Logout">
            <button type="submit" class="btn btn-success">Sign out</button>
          </form>
        </div><!--/.nav-collapse -->
      </div>
    </div>

    <div class="container">

      <div class="starter-template" style="position:relative; top:50px">
        <h1>Howdy, <%= request.getSession().getAttribute("first_name") %>! </h1>
        
        <p class="lead">It appears like this is your first time using this application..
        <br> Here's a video tutorial on how to use this app. Once you're ready, click 'Let's get started'</p>
        <div id='tutorial' align=center>
        	<embed
				width="420" height="345"
				src="http://www.youtube.com/v/IKqV7DB8Iwg"
				type="application/x-shockwave-flash">
			</embed>
			<p><br/><a class="btn btn-primary btn-lg" role="button" href="your_classes.jsp">Let's get started &raquo;</a></p>
			
        </div>        
      </div>

    </div><!-- /.container -->


    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://code.jquery.com/jquery-1.10.2.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
  </body>
</html>
