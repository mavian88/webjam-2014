<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ page import="java.io.*, java.sql.*, javax.servlet.*, javax.servlet.http.*, java.util.*" %>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="../../assets/ico/favicon.ico">

<script>

function autoResize(id){
    var newheight;
    var newwidth;

    if(document.getElementById){
        newheight=document.getElementById(id).contentWindow.document .body.scrollHeight;
        newwidth=document.getElementById(id).contentWindow.document .body.scrollWidth;
    }

    document.getElementById(id).height= (newheight) + "px";
    document.getElementById(id).width= (newwidth) + "px";
}
</script>

    <title>Home Page</title>

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.css" rel="stylesheet">

    <!-- Just for debugging purposes. Don't actually copy this line! -->
    <!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>

  <body>

    <div class="navbar navbar-inverse navbar-fixed-top" role="navigation" style="background-color:#101010">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="#">Pomodoro Buddy</a>
        </div>
        <div class="collapse navbar-collapse">
          <ul class="nav navbar-nav">
            <li class="active"><a href="#">Home</a></li>
            <li><a href="your_classes.jsp">Classes</a></li>
            <li><a href="your_tasks.jsp">Tasks</a></li>
            <li><a href="timer.html">Timer</a>
          </ul>
          <form class="navbar-form navbar-right" role="form" action="/WebJamProject/Logout">
            <button type="submit" class="btn btn-success">Sign out</button>
          </form>
        </div><!--/.nav-collapse -->
      </div>
    </div>

    <div class="container">
	  <%
		ArrayList<Integer> classes = (ArrayList<Integer>) request.getSession().getAttribute("classes");
  		ArrayList<Integer> tasks = (ArrayList<Integer>) request.getSession().getAttribute("tasks");
  		boolean print_schedule = false;
  		boolean classes_empty = classes.isEmpty();
  		boolean tasks_empty = tasks.isEmpty();	
	  %>
	  
      	<div style="position: fixed; top:40px; background-color: #FFFFFF; opacity: 1; width: 100%; z-index:10">
        	<h1>Welcome <%= request.getSession().getAttribute("first_name") %>! </h1>
        	
        	<%
	        	if(!classes_empty && !tasks_empty){
	        		out.println("<p class=\"lead\"> Here's your schedule for this week.. </p> ");
	        	}
        	%>
        	
        </div>
        
        <%
        	if(classes_empty && tasks_empty){
        			response.sendRedirect("new_user.jsp");
        	}
        	else if(tasks_empty){
        		out.print("<p class=\"lead\"><br/>You don't have any tasks right now..<br/> Would you like to create one?</p>" +
        					"<div id='tutorial' align=center>" +
								"<p><a class=\"btn btn-primary btn-lg\" role=\"button\" href=\"your_tasks.jsp\">Add tasks &raquo;</a></p>" +
        					"</div>");
        	}
        	else{
        		print_schedule = true;
        	}
        %>	
	<%
		if(print_schedule){
			out.print("<div id='schedule_pane' align='center' style='position: relative; top: 130px;'>" + 
					"<iframe width='100%' height='200px' id=\"iframe1\" marginheight=\"0\" frameborder=\"0\" onLoad=\"autoResize('iframe1');\" src=\"schedule.jsp\" sandbox='allow-top-navigation allow-same-origin allow-scripts'></iframe>" +
			  	  "<p><br/></p>  </div>");
		}
	%>
	
    </div><!-- /.container -->
    
    
    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://code.jquery.com/jquery-1.10.2.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
  </body>
</html>
