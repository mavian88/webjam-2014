<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ page import="java.io.*, java.sql.*, javax.servlet.*, javax.servlet.http.*, java.util.*" %>

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="../../assets/ico/favicon.ico">
    
<link rel='stylesheet' type='text/css' href='fullcalendar-1.6.4/fullcalendar/fullcalendar.css' />

<!-- Bootstrap core CSS -->
<link href="css/bootstrap.css" rel="stylesheet" />

<script type='text/javascript' src='fullcalendar-1.6.4/lib/jquery.min.js'></script>
<script type='text/javascript' src='fullcalendar-1.6.4/lib/jquery-ui.custom.min.js'></script>
<script type='text/javascript' src='fullcalendar-1.6.4/fullcalendar/fullcalendar.min.js'></script>
<script type='text/javascript'>
<% 
Random r = new Random();
String a = "456789AB";
String color = "";
%>
	$(document).ready(function() {
	
		/* initialize the external events
		-----------------------------------------------------------------*/
	
		$('.external-event').each(function() {
		
			// create an Event Object (http://arshaw.com/fullcalendar/docs/event_data/Event_Object/)
			// it doesn't need to have a start or end
			var eventObject = {
				title: $.trim($(this).text()), // use the element's text as the event title
				color: <% out.print("'#" + a.charAt(r.nextInt(a.length())) + a.charAt(r.nextInt(a.length())) + a.charAt(r.nextInt(a.length())) + "'");  %>
			};
			
			// store the Event Object in the DOM element so we can get to it later
			$(this).data('eventObject', eventObject);
			
			// make the event draggable using jQuery UI
			$(this).draggable({
				zIndex: 999,
				revert: true,      // will cause the event to go back to its
				revertDuration: 0  //  original position after the drag
			});
			
		});

		
		var date = new Date();
		var d = date.getDate();
		var dd = date.getDay();
		var m = date.getMonth();
		var y = date.getFullYear();
		
		$('#calendar').fullCalendar({
			editable: true,
			allDaySlot: false,
			firstDay: 1,
		    minTime: 8,
		    maxTime: 22,
			header: {
				left: 'none',
				center: 'title',
				right: 'none'
			},
			defaultView: 'agendaWeek',
			droppable: true,
			drop: function(date, allDay) { // this function is called when something is dropped
				if(date < new Date()){
					return;
				}
			
				// retrieve the dropped element's stored Event Object
				var originalEventObject = $(this).data('eventObject');
				
				// we need to copy it, so that multiple events don't have a reference to the same object
				var copiedEventObject = $.extend({}, originalEventObject);
				
				// assign it the date that was reported
				copiedEventObject.start = date;
				copiedEventObject.allDay = allDay;
				
				// render the event on the calendar
				// the last `true` argument determines if the event "sticks" (http://arshaw.com/fullcalendar/docs/event_rendering/renderEvent/)
				$('#calendar').fullCalendar('renderEvent', copiedEventObject, true);
				
				// is the "remove after drop" checkbox checked?
				if ($('#drop-remove').is(':checked')) {
					// if so, remove the element from the "Draggable Events" list
					$(this).remove();
				}
				
				var task_duration = prompt("Please enter the duration for this task (minutes):");
				window.location = "/WebJamProject/timer.jsp?description=" + $(this).data('eventObject').title + "&task-duration=" + task_duration;				
			},

			events: [
				<% 
					Connection dbcon = (Connection) request.getSession().getAttribute("connection");
					ArrayList<Integer> classes = (ArrayList<Integer>) request.getSession().getAttribute("classes");
					
					for(int c: classes){
						String sql = "SELECT * FROM classes WHERE id=?";
						PreparedStatement ps = dbcon.prepareStatement(sql);
						ps.setInt(1, c);
						
						ResultSet rs = ps.executeQuery();
						
						if(rs.next()){
							String name = rs.getString(2);
							
							String start_time = rs.getString(3);
							Time start_time_t = Time.valueOf(start_time);

							StringTokenizer st = new StringTokenizer(start_time, ":");
							int start_h = Integer.parseInt(st.nextToken());
							int start_m = Integer.parseInt(st.nextToken());
							
							String end_time = rs.getString(4);
							Time end_time_t = Time.valueOf(end_time);
							
							st = new StringTokenizer(end_time, ":");
							int end_h = Integer.parseInt(st.nextToken());
							int end_m = Integer.parseInt(st.nextToken());
							
							System.out.println(" " + end_time_t + " " + start_time_t);
							
							boolean mon = rs.getBoolean(5);
							boolean tue = rs.getBoolean(6);
							boolean wed = rs.getBoolean(7);
							boolean thu = rs.getBoolean(8);
							boolean fri = rs.getBoolean(9);
							
							color = "#" + a.charAt(r.nextInt(a.length())) + a.charAt(r.nextInt(a.length())) + a.charAt(r.nextInt(a.length()));
							
							if(mon){
								out.println("{ title: '" + name + "', start: new Date(y,m,d-dd+" + 1 + "," + start_h + "," + start_m + "), " + 
												"end: new Date(y,m,d-dd+" + 1 + "," + end_h + "," + end_m + "), color: '" + color + "', allDay: false, editable: false },");
							}
							if(tue){
								out.println("{ title: '" + name + "', start: new Date(y,m,d-dd+" + 2 + "," + start_h + "," + start_m + "), " + 
												"end: new Date(y,m,d-dd+" + 2 + "," + end_h + "," + end_m + "), color: '" + color + "', allDay: false, editable: false },");
							}
							if(wed){
								out.println("{ title: '" + name + "', start: new Date(y,m,d-dd+" + 3 + "," + start_h + "," + start_m + "), " + 
												"end: new Date(y,m,d-dd+" + 3 + "," + end_h + "," + end_m + "), color: '" + color + "', allDay: false, editable: false },");
							}
							if(thu){
								out.println("{ title: '" + name + "', start: new Date(y,m,d-dd+" + 4 + "," + start_h + "," + start_m + "), " + 
												"end: new Date(y,m,d-dd+" + 4 + "," + end_h + "," + end_m + "), color: '" + color + "', allDay: false, editable: false },");
							}
							if(fri){
								out.println("{ title: '" + name + "', start: new Date(y,m,d-dd+" + 5 + "," + start_h + "," + start_m + "), " + 
												"end: new Date(y,m,d-dd+" + 5 + "," + end_h + "," + end_m + "), color: '" + color + "', allDay: false, editable: false },");
							}
							
							
						}
					}
					
				%>
			]
		});
		
	});

</script>
<style type='text/css'>

	body {
		margin-top: 40px;
		font-family: "Lucida Grande",Helvetica,Arial,Verdana,sans-serif;
		}

	#calendar {
		width: 850px;
		margin: 0 auto;
		font-size: 12px;
		}
	div.inline { float:left; }
	
</style>

    <!-- Just for debugging purposes. Don't actually copy this line! -->
    <!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>
<body>

    <div class="navbar navbar-inverse navbar-fixed-top" role="navigation" style="background-color:#101010; z-index:10">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="#">Team Pomodori</a>
        </div>
        <div class="collapse navbar-collapse">
          <ul class="nav navbar-nav">
            <li class="active"><a href="#">Overview</a></li>
            <li><a href="your_classes.jsp">Classes</a></li>
            <li><a href="your_tasks.jsp">Tasks</a></li>
            <li><a href="timer.jsp">Timer</a>
          </ul>
          <form class="navbar-form navbar-right" role="form" action="/WebJamProject/Logout">
            <button type="submit" class="btn btn-success">Sign out</button>
          </form>
        </div><!--/.nav-collapse -->
      </div>
    </div>
    
    <div class="container">
	  <%
//		ArrayList<Integer> classes = (ArrayList<Integer>) request.getSession().getAttribute("classes");
  		ArrayList<Integer> tasks = (ArrayList<Integer>) request.getSession().getAttribute("tasks");
  		boolean print_schedule = false;
  		boolean classes_empty = classes.isEmpty();
  		boolean tasks_empty = tasks.isEmpty();	
	  %>
	  
      <div style="position:relative; z-index:10; opacity:1">
      	<div style="position: fixed; top:52px;  background-color: #FFFFFF; width:100%">
        	<h1>Welcome <%= request.getSession().getAttribute("first_name") %>! </h1>
        	
        	<%
	        	if(!classes_empty && !tasks_empty){
	        		out.println("<p class=\"lead\"> Here's your schedule for this week.. </p> ");
	        	}
        	%>
        	
        </div>
        
        <%
        	if(classes_empty && tasks_empty){
        			response.sendRedirect("new_user.jsp");
        	}
        	else if(tasks_empty){
        		out.print("<div style='position:relative; top:100px'> <p class=\"lead\"><br/>You don't have any tasks right now..<br/> Would you like to create one?</p>" +
        					"<div id='tutorial' align=center>" +
								"<p><a class=\"btn btn-primary btn-lg\" role=\"button\" href=\"your_tasks.jsp\">Add tasks &raquo;</a></p>" +
        					"</div></div>");
        	}
        	else{
 //       		out.print("<div id='schedule_pane' align='center' style='position: relative; top: 40px; z-index:-1'>" + 
 //   						"<iframe width='100%' height='200px' id=\"iframe1\" marginheight=\"0\" frameborder=\"0\" onLoad=\"autoResize('iframe1');\" src=\"schedule.jsp\" sandbox='allow-scripts allow-same-origin'></iframe>" +
 //   				  	  "<p><br/></p>  </div>");
        	}
        %>
      
      </div>
      <% if(!classes_empty && !tasks_empty){ %>
      
      <div id='schedule' style='position:relative; top:130px'>
      	<div class='inline' id='calendar'></div>
		<div class='inline' id='tasks'>
			<%
		//		ArrayList<Integer> tasks = (ArrayList<Integer>) request.getSession().getAttribute("tasks");
				
				if(!tasks.isEmpty()){
					String task_list = "";
		   			for(int task_id : tasks){
		   				task_list = task_list + (", " + task_id);	
					}
		   			task_list = task_list.substring(2);
		   			String sql = "SELECT description FROM tasks WHERE id IN (" + task_list + ") AND is_completed=0 ORDER BY is_completed, due_date DESC";
					PreparedStatement ps = dbcon.prepareStatement(sql);
					ResultSet rs = ps.executeQuery();
					
					out.print("<div class='panel panel-default' style='position:relative; margin-top:75%; left:20px; background-color:#FFFFBB'>" +
									"<table id='todo_list' class='table' style='text-align:left; border-collapse:separate; border-spacing:5px'> <th> To-Do List </th>");
						
					while(rs.next()){
						out.print("<tr><td><div class='external-event'>" + rs.getString(1) + "</div></td></tr>");
					}		
					out.print("</table></div></div>");
				}
		
			%>
		</div>
	</div>
	<% } %>
	
    </div><!-- /.container -->
    
</body>
</html>
