<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ page import="java.io.*, java.sql.*, javax.servlet.*, javax.servlet.http.*, java.util.*" %>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="../../assets/ico/favicon.ico">
    
    <style>
    	#class_table td {font-size:12px; text-align:center}
    	th {width:20%}
    	.th {text-align:center}
    </style>

    <title>Classes Page</title>
    
    <!-- Pure Form CSS -->
    <link rel="stylesheet" href="http://yui.yahooapis.com/pure/0.4.1/pure-min.css">

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.css" rel="stylesheet">


    <!-- Just for debugging purposes. Don't actually copy this line! -->
    <!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>

  <body>

    <div class="navbar navbar-inverse navbar-fixed-top" role="navigation" style="background-color:#101010">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="#">Team Pomodori</a>
        </div>
        <div class="collapse navbar-collapse">
          <ul class="nav navbar-nav">
            <li><a href="overview.jsp">Overview</a></li>
            <li class="active"><a href="#">Classes</a></li>
            <li><a href="your_tasks.jsp">Tasks</a></li>
            <li><a href="timer.jsp">Timer</a>
          </ul>
          <form class="navbar-form navbar-right" role="form" action="/WebJamProject/Logout">
            <button type="submit" class="btn btn-success">Sign out</button>
          </form>
        </div><!--/.nav-collapse -->
      </div>
    </div>

    <div class="container">

		<div align="center" style="position:relative; top:50px">   
			 <form class="pure-form pure-form-stacked" action="/WebJamProject/AddClass" method="get">
			    <fieldset>
			        <legend><strong>Class Input Form</strong></legend>
			
			        <div class="pure-g">
			        	<table style="border-collapse:separate; border-spacing:20px" id="class_form">
			        	<tr>
			        	<td>
			            <div class="pure-u-2 pure-u-med-1-3">
			                <label for="class-name" class="pure-u-1 pure-u-med-1-3">Class Name</label>
			                <input id="class-name" type="text" name="class_name" required>
			            </div>
						</td>
						<td>
			            <div class="pure-u-2 pure-u-med-1-3">
			                <label for="start_time" class="pure-u-1 pure-u-med-1-3">Start Time</label>
			                <select id="start_time" class="pure-input-1-1" required name="start_time">
								<option>8:00</option>
			                    <option>8:30</option>
			                    <option>9:00</option>
			                    <option>9:30</option>
			                    <option>10:00</option>
			                    <option>10:30</option>
			                    <option>11:00</option>
			                    <option>11:30</option>
			                    <option>12:00</option>
			                    <option>12:30</option>
			                    <option>13:00</option>
			                    <option>13:30</option>
			                    <option>14:00</option>
			                    <option>14:30</option>
			                    <option>15:00</option>
			                    <option>15:30</option>
			                    <option>16:00</option>
			                    <option>16:30</option>
			                    <option>17:00</option>
			                    <option>17:30</option>
			                    <option>18:00</option>
			                    <option>18:30</option>
			                    <option>19:00</option>
			                    <option>19:30</option>
			                    <option>20:00</option>
			                    <option>20:30</option>
			                    <option>21:00</option>
			                    <option>21:30</option>
			                    <option>22:00</option>
			                </select>
			            </div>
						</td>
						<td>
			            <div class="pure-u-2 pure-u-med-1-3">
			                <label for="end_time" class="pure-u-1 pure-u-med-1-3">End Time</label>
			                <select id="end_time" class="pure-input-1-1" required name="end_time">
			                    <option>8:00</option>
			                    <option>8:30</option>
			                    <option>9:00</option>
			                    <option>9:30</option>
			                    <option>10:00</option>
			                    <option>10:30</option>
			                    <option>11:00</option>
			                    <option>11:30</option>
			                    <option>12:00</option>
			                    <option>12:30</option>
			                    <option>13:00</option>
			                    <option>13:30</option>
			                    <option>14:00</option>
			                    <option>14:30</option>
			                    <option>15:00</option>
			                    <option>15:30</option>
			                    <option>16:00</option>
			                    <option>16:30</option>
			                    <option>17:00</option>
			                    <option>17:30</option>
			                    <option>18:00</option>
			                    <option>18:30</option>
			                    <option>19:00</option>
			                    <option>19:30</option>
			                    <option>20:00</option>
			                    <option>20:30</option>
			                    <option>21:00</option>
			                    <option>21:30</option>
			                    <option>22:00</option>
			                </select>
			            </div>
			            </td>
			            <td valign="top">
			            <div class="pure-u-2 pure-u-med-1-3">
			            	<label for="monday" class="pure-u-1 pure-u-med-1-3"> Mon </label>
					            <input id="monday" type="checkbox" name="monday"> 
					    </div>
			            </td>
			            <td valign="top">
			            <div class="pure-u-2 pure-u-med-1-3">
			            	<label for="tuesday" class="pure-u-1 pure-u-med-1-3"> Tue </label>
					            <input id="tuesday" type="checkbox" name="tuesday"> 
					    </div>
			            </td>
			             <td valign="top">
			            <div class="pure-u-2 pure-u-med-1-3">
			            	<label for="wednesday" class="pure-u-1 pure-u-med-1-3"> Wed </label>
					            <input id="wednesday" type="checkbox" name="wednesday"> 
					    </div>
			            </td>
			             <td valign="top">
			            <div class="pure-u-2 pure-u-med-1-3">
			            	<label for="thursday" class="pure-u-1 pure-u-med-1-3"> Thu </label>
					            <input id="thursday" type="checkbox" name="thursday"> 
					    </div>
			            </td>
			            <td valign="top">
			            <div class="pure-u-2 pure-u-med-1-3">
			            	<label for="friday" class="pure-u-1 pure-u-med-1-3"> Fri </label>
					            <input id="friday" type="checkbox" name="friday"> 
					    </div>
			            </td>
			          	<td>
			          		<button type="submit" class="btn btn-primary btn-sm">Add This Class</button>
			          	</td>
			            </tr>
			            </table>
			        </div>
			    </fieldset>
			</form>	  
		</div>

      <div class="starter-template" style="position:relative; top:20px">
        <h1><%= request.getSession().getAttribute("first_name") %>'s Classes! </h1>
        
        <% 
        	ArrayList<Integer> classes = (ArrayList<Integer>) request.getSession().getAttribute("classes");
        	if(classes.isEmpty()) {
        		out.print("<p class=\"lead\">No classes in here! <br> Let's add some..</p>");
        	}
	        else {
	        	Connection dbcon = (Connection) request.getSession().getAttribute("connection");        	
	        	
	        	out.println("<div class='panel panel-default' style='position:relative; left:20px'>" +
	        				"<div class='panel-heading'>Here are your classes: </div>" +
	        				"<table id='class_table'class='table' style='border-collapse:separate; border-spacing:5px'>" +
	        				"<tr><th class='th'>Mon</th><th class='th'>Tue</th><th class='th'>Wed</th><th class='th'>Thu</th><th class='th'>Fri</th></tr>");
	   			for(int class_id : classes){
	   				
	   				String sql = "SELECT * FROM classes WHERE id=?";
	   				PreparedStatement ps = dbcon.prepareStatement(sql);
	   				ps.setInt(1, class_id);
	   				ResultSet rs = ps.executeQuery();
	   				
	   				if(rs.next()){
	   					String name = rs.getString(2);
	   					String start_time = (new java.text.SimpleDateFormat("H:mm").format(rs.getTime(3))).toString();
	   					String end_time = (new java.text.SimpleDateFormat("H:mm").format(rs.getTime(4))).toString();
	   	        		boolean mon = rs.getBoolean(5);
	   	        		boolean tue = rs.getBoolean(6);
	   	        		boolean wed = rs.getBoolean(7);
	   	        		boolean thu = rs.getBoolean(8);
	   	        		boolean fri = rs.getBoolean(9);
	   					
	   	        		out.println("<tr>");
		   	        	out.println( (mon) ? "<td><div class='class_info'>" + name + "<br/>" + start_time + " - " + end_time + "</div></td>" : "<td> &nbsp; </td>" );
		   	        	out.println( (tue) ? "<td><div class='class_info'>" + name + "<br/>" + start_time + " - " + end_time + "</div></td>" : "<td> &nbsp; </td>" );
		   	        	out.println( (wed) ? "<td><div class='class_info'>" + name + "<br/>" + start_time + " - " + end_time + "</div></td>" : "<td> &nbsp; </td>" );
		   	        	out.println( (thu) ? "<td><div class='class_info'>" + name + "<br/>" + start_time + " - " + end_time + "</div></td>" : "<td> &nbsp; </td>" );
		   	        	out.println( (fri) ? "<td><div class='class_info'>" + name + "<br/>" + start_time + " - " + end_time + "</div></td>" : "<td> &nbsp; </td>" );
	
		   	        	out.println("</tr>");
	   				}
	   			}
	   			out.println("</table></div>");
	        }
        %>
     
	     
	   <%
			if(!classes.isEmpty()){
				out.print("<div id='tutorial' align=center>" +		
						"<p><strong>Next: &nbsp; &nbsp;</strong> <a class=\"btn btn-primary btn-lg\" role=\"button\" href=\"your_tasks.jsp\">Add tasks &raquo;</a></p>" +	
				        "</div>" +        
				      "</div>");
			}
	   %> 


    </div><!-- /.container -->


    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://code.jquery.com/jquery-1.10.2.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
  </body>
</html>
