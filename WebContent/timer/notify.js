/*
* notify.js - by Pratik Desai {desai@pratyk.com}
* Enables Chrome Desktop Notification support for use with jQuery plugin - The Final Countdown + Audio Notifications using <audio> tag
* Modified from: http://0xfe.blogspot.com/2010/04/desktop-notifications-with-webkit.html
*/

function Notifier() {}
    Notifier.prototype.HasSupport = function() {
      if (window.webkitNotifications) {
        return true;
      } else {
        return false;
      }
    };
    Notifier.prototype.RequestPermission = function(cb) {
      window.webkitNotifications.requestPermission(function() {
        if (cb) { cb(window.webkitNotifications.checkPermission() == 0); }
      });
    };
    Notifier.prototype.Notify = function(icon, title, body) {
      if (window.webkitNotifications.checkPermission() == 0) {
        var popup = window.webkitNotifications.createNotification(
        icon, title, body);
        popup.show();
		setTimeout(function(){ popup.cancel(); }, '11000');
        return true;
      }
      return false;
    };
    $(function() {
      var notifier = new Notifier();
      if (!notifier.HasSupport()) {
        $("#error").show();
        return;
      }
      
      $("#request-permission").click(function() {
        $("#error").hide();
        notifier.RequestPermission();
      });
    });
    
    function desktopAlert(){
    var notifier = new Notifier();
     if (!notifier.HasSupport()) {
        $("#error").show();
        return;
      }
    if (!notifier.Notify("/img/logo.png", "TomatoTimer", "The time is up!")) {
          $("#error").text('Permission denied. Click "Enable Desktop Notifications" to give us access to send notifications to your desktop.');
          $("#error").show();
        } else {
          $("#error").hide();
        }
    }
    
    function buzzer() {
        var alertoption = localStorage.getItem("alertoption");
        var volumeoption = localStorage.getItem("volumeoption");
        var choice = new Array();
        choice[0]= alertoption+'.mp3';
        choice[1]= alertoption+'.ogg';
        choice[2]= alertoption+'.wav';

        var alarm = new Howl({
            urls: choice,
            volume: volumeoption
        });
		alarm.play();
	}