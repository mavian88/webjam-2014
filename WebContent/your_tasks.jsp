<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ page import="java.io.*, java.sql.*, javax.servlet.*, javax.servlet.http.*, java.util.*" %>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="../../assets/ico/favicon.ico">
    <style>
    	#task_table td {font-size:12px; text-align:center}
    	th {width:25%}
    	.th {text-align:center}
    </style>
    <title>Tasks Page</title>

    <!-- Pure Form CSS -->
    <link rel="stylesheet" href="http://yui.yahooapis.com/pure/0.4.1/pure-min.css">
    
    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.css" rel="stylesheet">

    <!-- Just for debugging purposes. Don't actually copy this line! -->
    <!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>

  <body>

    <div class="navbar navbar-inverse navbar-fixed-top" role="navigation" style="background-color:#101010">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="#">Team Pomodori</a>
        </div>
        <div class="collapse navbar-collapse">
          <ul class="nav navbar-nav">
            <li><a href="overview.jsp">Overview</a></li>
            <li><a href="your_classes.jsp">Classes</a></li>
            <li class="active"><a href="#">Tasks</a></li>
            <li><a href="timer.jsp">Timer</a>
          </ul>
          <form class="navbar-form navbar-right" role="form" action="/WebJamProject/Logout">
            <button type="submit" class="btn btn-success">Sign out</button>
          </form>
        </div><!--/.nav-collapse -->
      </div>
    </div>

    <div class="container">

		<div align="center" style="position:relative; top:50px">   
			 <form class="pure-form pure-form-stacked" action="/WebJamProject/AddTask" method="get">
			    <fieldset>
			        <legend><strong>Task Input Form</strong></legend>
			
			        <div class="pure-g">
			        	<table style="border-collapse:separate; border-spacing:20px" id="class_form">
			        	<tr>
			        	<td valign="top">
			            <div class="pure-u-2 pure-u-med-1-3">
			                <label for="for_class" class="pure-u-1 pure-u-med-1-3">For Which Class</label>
			                <select id="for_class" class="pure-input-1-1" name="for_class" required>
							<%
								Connection dbcon = (Connection) request.getSession().getAttribute("connection");
			                	ArrayList<Integer> classes = (ArrayList<Integer>) request.getSession().getAttribute("classes");
			                		
			                	if(classes.isEmpty()){
			                		response.sendRedirect("new_user.jsp");	
			                		return;
			                	}
			                	
			                	String sql = "SELECT name FROM classes WHERE id=?";
			                	PreparedStatement ps = dbcon.prepareStatement(sql);
			                	
			                	for(int class_id : classes){
			                		ps.setInt(1, class_id);
			                		ResultSet rs = ps.executeQuery();
			                		
			                		if(rs.next()){
			                			out.print("<option>" + rs.getString(1) + "</option>");
			                		}
			                	}
							%>
			                </select>
			            </div>
						</td>
			        	<td valign="top">
			            <div class="pure-u-2 pure-u-med-1-3">
			                <label for="task_desc" class="pure-u-1 pure-u-med-1-3">Task Description</label>
			                <input id="task_desc" class="pure-u-1 pure-input-1-1" type="text" name="description" required>
			            </div>
						</td>
						<td valign="top">
			            <!--  <div class="pure-u-2 pure-u-med-1-3">
			                <label for="task_dur" class="pure-u-1 pure-u-med-1-3">Task Duration (Hours)</label>
			                <select id="task_dur" class="pure-input-1-1" name="duration" required>
								<option>0.50</option>
			                    <option>1.00</option>
			                    <option>1.50</option>
			                    <option>2.00</option>
			                    <option>2.50</option>
			                    <option>3.00</option>
			                    <option>3.50</option>
			                    <option>4.00</option>
			                    <option>4.50</option>
			                    <option>5.00</option>
			                </select>
			            </div> -->
						</td>
						<td valign="top">
			            <div class="pure-u-2 pure-u-med-1-3">
			                <label for="task_due" class="pure-u-1 pure-u-med-1-3">Task Due Date</label>
			                <input id="task_due" type="date" name="due_date" required>
			            </div>
			            </td>
			          	<td>
			          		<button type="submit" class="btn btn-primary btn-sm">Add This Task</button>
			          	</td>
			            </tr>
			            </table>
			        </div>
			    </fieldset>
			</form>	  
		</div>

      <div class="starter-template" style="position:relative; top:20px">
        <h1><%= request.getSession().getAttribute("first_name") %>'s Tasks! </h1>
        
        <%
	        ArrayList<Integer> tasks = (ArrayList<Integer>) request.getSession().getAttribute("tasks");
        	if(!classes.isEmpty()) {
	    		if(tasks.isEmpty()){
		    		out.print("<p class=\"lead\">No tasks in here! <br> Let's add some..</p>");
	    		}
	    		else{
		        	out.println("<div class='panel panel-default' style='position:relative; left:20px'>" +
	        				"<div class='panel-heading'>Here are your tasks: </div>" +
	        				"<table id='task_table'class='table' style='border-collapse:separate; border-spacing:5px'>" +
	        				"<tr><th class='th'>Description</th><th class='th'>Due Date</th><th class='th'>Finished?</th></tr>");
					
		        	String task_list = "";
		   			for(int task_id : tasks){
		   				task_list = task_list + (", " + task_id);	
	    			}
		   			task_list = task_list.substring(2);
		   			sql = "SELECT description, due_date, is_completed FROM tasks WHERE id IN (" + task_list + ") ORDER BY is_completed, due_date";
	   				ps = dbcon.prepareStatement(sql);
	   				ResultSet rs = ps.executeQuery();
	   				
	   				while(rs.next()){
	   					String description = rs.getString(1);
	   					String due_date = rs.getString(2);
	   					String finished = rs.getBoolean(3) ? "Yes" : "No";
	   					
	   	        		out.println("<tr>");
		   	        	out.println("<td><div class='task_info'>" + description + "</div></td>");
		   	        	out.println("<td><div class='task_info'>" + due_date + "</div></td>");
		   	        	out.println("<td><div class='task_info'>" + finished + "</div></td>");
		   	        	out.println("</tr>");
	   				}
	   			}
	        	
	        	out.println("</table></div>");
	    	}        
        %>
        
        
        <%
			if(!tasks.isEmpty()){
				out.print("<div id='tutorial' align=center>" +		
						"<p><strong>You're all set! &nbsp; &nbsp;</strong> <a class=\"btn btn-primary btn-lg\" role=\"button\" href=\"overview.jsp\">See Schedule &raquo;</a></p>" +	
				        "</div>" +        
				      "</div>");
			}
	    %> 
	        
      </div>
	
    </div><!-- /.container -->

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://code.jquery.com/jquery-1.10.2.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
  </body>
</html>
