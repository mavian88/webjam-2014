

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.sql.*;

/**
 * Servlet implementation class CompleteTask
 */
public class CompleteTask extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public CompleteTask() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String description = request.getParameter("description");
		
		Connection dbcon = (Connection) request.getSession().getAttribute("connection");
		
		String sql = "SELECT id from tasks WHERE description='" + description + "'";
		try {
			PreparedStatement ps = dbcon.prepareStatement(sql);
			ResultSet rs = ps.executeQuery();
			
			if(rs.next()){
				int task_id = rs.getInt(1);
				sql = "UPDATE tasks SET is_completed=1 WHERE id=" + task_id;
				ps = dbcon.prepareStatement(sql);
				ps.executeUpdate();
				
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		
		response.sendRedirect("overview.jsp");
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
