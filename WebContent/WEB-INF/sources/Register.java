

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class Register
 */
public class Register extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Register() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see Servlet#getServletInfo()
	 */
	public String getServletInfo() {
		// TODO Auto-generated method stub
		return "Servlet for registering"; 
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String loginUser = "root";
        String loginPasswd = "";
        String loginUrl = "jdbc:mysql://localhost:3306/webappdb";
        
       //Class.forName("org.gjt.mm.mysql.Driver");
        try {
			Class.forName("com.mysql.jdbc.Driver").newInstance();
	        Connection dbcon = DriverManager.getConnection(loginUrl, loginUser, loginPasswd);
            
            String email = request.getParameter("email");
//            if(!email.contains("@uci.edu")){
//            	response.sendRedirect("register.html");
//            	return;
//            }
            
            String password = request.getParameter("password");
            String first_name = request.getParameter("first_name");
            String last_name = request.getParameter("last_name");
            
            String query = "SELECT email FROM users WHERE email=?";
            
			PreparedStatement statement = dbcon.prepareStatement(query);
			statement.setString(1, email);
			ResultSet login = statement.executeQuery();
			
			if(email != null){

				if(!login.next()){
					query = "INSERT INTO users (email, password, first_name, last_name) VALUES (?,?,?,?)";
					
					statement = dbcon.prepareStatement(query);
					statement.setString(1, email);
					statement.setString(2, password);
					statement.setString(3, first_name);
					statement.setString(4, last_name);
					statement.executeUpdate();	
					
					//fetch the newly-created id
					query = "SELECT id FROM users WHERE email=?";
					statement = dbcon.prepareStatement(query);
					statement.setString(1, email);
					ResultSet rs = statement.executeQuery();
					rs.next();
					int id = rs.getInt(1);
					
					//initialize the session
					request.getSession().setAttribute("id", id);
					request.getSession().setAttribute("first_name", first_name);
					request.getSession().setAttribute("last_name", last_name);
					request.getSession().setAttribute("email", email);
//					request.getSession().setAttribute("password", pw);
					
					request.getSession().setAttribute("connection", dbcon);
					request.getSession().setAttribute("loggedin", "true");
					request.getSession().setAttribute("classes", new ArrayList<Integer>());
					request.getSession().setAttribute("tasks", new ArrayList<Integer>());

					response.sendRedirect("new_user.jsp");
				}
				else{
					response.sendRedirect("register.html");
				}
			}
			
			login.close();
            statement.close();

			
		} catch (InstantiationException | IllegalAccessException | ClassNotFoundException | SQLException e) {
			e.printStackTrace();
		}

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
