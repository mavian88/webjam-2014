

import java.io.IOException;
import java.sql.*;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class AddClass
 */
public class AddClass extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public AddClass() {
        super();
    }

	/**
	 * @see Servlet#getServletInfo()
	 */
	public String getServletInfo() {
		return "Servlet for adding classes"; 
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	@SuppressWarnings("unchecked")
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
				
		Connection dbcon = (Connection) request.getSession().getAttribute("connection");
		
		String class_name = request.getParameter("class_name");
		String start_time = request.getParameter("start_time");
		String end_time = request.getParameter("end_time");
		boolean monday = (request.getParameter("monday") == null) ? false : true;
		boolean tuesday = (request.getParameter("tuesday") == null) ? false : true;
		boolean wednesday = (request.getParameter("wednesday") == null) ? false : true;
		boolean thursday = (request.getParameter("thursday") == null) ? false : true;
		boolean friday = (request.getParameter("friday") == null) ? false : true;
		
		if(class_name == "" || start_time == "" || end_time == "" || 
			(!monday && !tuesday && !wednesday && !thursday && !friday)){
			response.sendRedirect("your_classes.jsp");
		} else {
			String sql = "INSERT INTO classes (name, start_time, end_time, monday, tuesday, wednesday, thursday, friday) "
							+ "VALUES ('" + class_name + "','" + start_time + "','" + end_time + "',"
							+ monday + "," + tuesday + "," + wednesday + "," + thursday + "," + friday + ")";
			try {
				PreparedStatement ps = dbcon.prepareStatement(sql);
				ps.executeUpdate();
				
				//for newly created class
				sql = "SELECT id FROM classes WHERE name='" + class_name + "'";
				ps = dbcon.prepareStatement(sql);
				ResultSet rs = ps.executeQuery();
				int class_id = -1;
				
				while(rs.next()){
					class_id = rs.getInt(1);
				}
				
				int user_id = (Integer) request.getSession().getAttribute("id");
				sql = "INSERT INTO users_classes VALUES(" + user_id + "," + class_id + ")";
				ps = dbcon.prepareStatement(sql);
				ps.executeUpdate();
				
				ArrayList<Integer> classes = new ArrayList<Integer>();
				
				sql = "SELECT id FROM classes WHERE id IN (SELECT class_id from users_classes WHERE user_id=?) ORDER BY start_time";
				ps = dbcon.prepareStatement(sql);
				int id = (Integer) request.getSession().getAttribute("id");
				ps.setInt(1, id);
				rs = ps.executeQuery();
				
				while(rs.next()){
					classes.add(rs.getInt(1));
				}
				
				
				request.getSession().setAttribute("classes", classes);
				response.sendRedirect("your_classes.jsp");
			} catch (SQLException e) {
				e.printStackTrace();
				response.sendRedirect("your_classes.jsp");
			}
		}

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
