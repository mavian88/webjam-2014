

import java.io.IOException;
import java.sql.*;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class Login
 */
public class Login extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Login() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see Servlet#getServletInfo()
	 */
	public String getServletInfo() {
		return "Servlet for Logging in"; 
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String loginUser = "root";
        String loginPasswd = "";
        String loginUrl = "jdbc:mysql://localhost:3306/webappdb";
        
       //Class.forName("org.gjt.mm.mysql.Driver");
        try {
			Class.forName("com.mysql.jdbc.Driver").newInstance();
	        Connection dbcon = DriverManager.getConnection(loginUrl, loginUser, loginPasswd);
            
            String email = request.getParameter("email");
            String password = request.getParameter("password");
            String query = "SELECT * FROM users WHERE email=?";
            
			PreparedStatement ps = dbcon.prepareStatement(query);
			
			ps.setString(1, email);
			
			ResultSet rs = ps.executeQuery();
			
			if(email != null){
				if(rs.next()){
					int id = rs.getInt(1);
					String first = rs.getString(4);
					String last = rs.getString(5);

					String pw = rs.getString(3);
					
					//initialize the session
					request.getSession().setAttribute("id", id);
					request.getSession().setAttribute("first_name", first);
					request.getSession().setAttribute("last_name", last);
					request.getSession().setAttribute("email", email);
//					request.getSession().setAttribute("password", pw);
					
					if(pw.equals(password)){
						request.getSession().setAttribute("connection", dbcon);
//						request.getSession().setAttribute("loggedin", "true");
						
						query = "SELECT id from classes WHERE id IN (SELECT class_id FROM users_classes where user_id=?) ORDER BY start_time";
						ps = dbcon.prepareStatement(query);
						ps.setInt(1, id);
						rs = ps.executeQuery();
						ArrayList<Integer> classes = new ArrayList<Integer>();
						while(rs.next()){
							classes.add(rs.getInt(1));
						}
						
						query = "SELECT id from tasks WHERE id IN (SELECT task_id FROM users_tasks where user_id=?) ORDER BY due_date";
						ps = dbcon.prepareStatement(query);
						ps.setInt(1, id);
						rs = ps.executeQuery();
						ArrayList<Integer> tasks = new ArrayList<Integer>();
						while(rs.next()){
							tasks.add(rs.getInt(1));
						}
						
						request.getSession().setAttribute("classes", classes);
						request.getSession().setAttribute("tasks", tasks);

						response.sendRedirect("overview.jsp");
					}
					else{
						response.sendRedirect("login.html");
					}
				}
				else{
					response.sendRedirect("login.html");
				}
			}
			
			rs.close();
            ps.close();

			
		} catch (InstantiationException | IllegalAccessException | ClassNotFoundException | SQLException e) {
			e.printStackTrace();
		}

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
