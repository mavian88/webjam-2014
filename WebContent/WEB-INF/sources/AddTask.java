

import java.io.IOException;
import java.sql.*;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class AddClass
 */
public class AddTask extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public AddTask() {
        super();
    }

	/**
	 * @see Servlet#getServletInfo()
	 */
	public String getServletInfo() {
		return "Servlet for adding tasks"; 
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	@SuppressWarnings("unchecked")
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
				
		Connection dbcon = (Connection) request.getSession().getAttribute("connection");
		
		String description = request.getParameter("description");
//		double duration = Double.parseDouble(request.getParameter("duration"));
		
		String due_date = request.getParameter("due_date");
		try {
			DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
			java.util.Date due = (java.util.Date) df.parse(due_date);
			java.util.Date curr = (java.util.Date) df.parse(df.format(Calendar.getInstance().getTime()));
			if(due.before(curr)){
				response.sendRedirect("your_tasks.jsp");
				return;
			}
		} catch (ParseException e1) {
			e1.printStackTrace();
		}
		
		try {
			String sql = "SELECT id FROM classes WHERE name=?";
			PreparedStatement ps = dbcon.prepareStatement(sql);
			ps.setString(1, request.getParameter("for_class"));
			
			ResultSet rs = ps.executeQuery();
			int class_id = -1;
			if(rs.next()){
				class_id = rs.getInt(1);
			}

			if(class_id == -1){
				response.sendRedirect("your_tasks.jsp");
			}
			
			sql = "INSERT INTO tasks (description, due_date)"
							+ "VALUES ('" + description + "','" + due_date + "')";

			ps = dbcon.prepareStatement(sql);
			ps.executeUpdate();
			
			//for newly created class
			sql = "SELECT id FROM tasks WHERE description='" + description + "'";
			ps = dbcon.prepareStatement(sql);
			rs = ps.executeQuery();
			int task_id = -1;
			
			while(rs.next()){
				task_id = rs.getInt(1);
			}
			
			int user_id = (Integer) request.getSession().getAttribute("id");
			sql = "INSERT INTO users_tasks VALUES(" + user_id + "," + task_id + ")";
			ps = dbcon.prepareStatement(sql);
			ps.executeUpdate();
			
			sql = "INSERT INTO tasks_classes VALUES(" + task_id + "," + class_id + ")";
			ps = dbcon.prepareStatement(sql);
			ps.executeUpdate();
			
			ArrayList<Integer> tasks = new ArrayList<Integer>();
			
			sql = "SELECT id FROM tasks WHERE id IN (SELECT task_id from users_tasks WHERE user_id=?) ORDER BY due_date";
			ps = dbcon.prepareStatement(sql);
			
			ps.setInt(1, (Integer) request.getSession().getAttribute("id"));
			rs = ps.executeQuery();
			
			while(rs.next()){
				tasks.add(rs.getInt(1));
			}
			
			
			request.getSession().setAttribute("tasks", tasks);
			response.sendRedirect("your_tasks.jsp");
			return;
		} catch (SQLException e) {
			response.sendRedirect("your_tasks.jsp");
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
