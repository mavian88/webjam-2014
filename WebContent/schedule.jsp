<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ page import="java.io.*, java.sql.*, javax.servlet.*, javax.servlet.http.*, java.util.*" %>

<!DOCTYPE html>
<html>
<head>
<link rel='stylesheet' type='text/css' href='fullcalendar-1.6.4/fullcalendar/fullcalendar.css' />

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.css" rel="stylesheet" />

<script type='text/javascript' src='fullcalendar-1.6.4/lib/jquery.min.js'></script>
<script type='text/javascript' src='fullcalendar-1.6.4/lib/jquery-ui.custom.min.js'></script>
<script type='text/javascript' src='fullcalendar-1.6.4/fullcalendar/fullcalendar.min.js'></script>
<script type='text/javascript'>

	$(document).ready(function() {
	
		/* initialize the external events
		-----------------------------------------------------------------*/
	
		$('div.external-event').each(function() {
		
			// create an Event Object (http://arshaw.com/fullcalendar/docs/event_data/Event_Object/)
			// it doesn't need to have a start or end
			var eventObject = {
				title: $.trim($(this).text()) // use the element's text as the event title
			};
			
			// store the Event Object in the DOM element so we can get to it later
			$(this).data('eventObject', eventObject);
			
			// make the event draggable using jQuery UI
			$(this).draggable({
				zIndex: 999,
				revert: true,      // will cause the event to go back to its
				revertDuration: 0  //  original position after the drag
			});
			
		});

		
		var date = new Date();
		var d = date.getDate();
		var dd = date.getDay();
		var m = date.getMonth();
		var y = date.getFullYear();
		
		$('#calendar').fullCalendar({
			editable: true,
			allDaySlot: false,
			firstDay: 1,
		    minTime: 8,
		    maxTime: 22,
			header: {
				left: 'none',
				center: 'title',
				right: 'none'
			},
			defaultView: 'agendaWeek',
			droppable: true,
			drop: function(date, allDay) { // this function is called when something is dropped
				if(date < new Date()){
					return;
				}
				
				// retrieve the dropped element's stored Event Object
				var originalEventObject = $(this).data('eventObject');
				
				// we need to copy it, so that multiple events don't have a reference to the same object
				var copiedEventObject = $.extend({}, originalEventObject);
				
				// assign it the date that was reported
				copiedEventObject.start = date;
				copiedEventObject.allDay = allDay;
				
				// render the event on the calendar
				// the last `true` argument determines if the event "sticks" (http://arshaw.com/fullcalendar/docs/event_rendering/renderEvent/)
				$('#calendar').fullCalendar('renderEvent', copiedEventObject, true);
				
				// is the "remove after drop" checkbox checked?
				if ($('#drop-remove').is(':checked')) {
					// if so, remove the element from the "Draggable Events" list
					$(this).remove();
				}
				
				var task_duration = prompt("Please enter the duration for this task:");
				
				window.parent.window.location = "timer.html?task-duration=" + task_duration;
			},

			events: [
				<% 
					Connection dbcon = (Connection) request.getSession().getAttribute("connection");
					ArrayList<Integer> classes = (ArrayList<Integer>) request.getSession().getAttribute("classes");
					
					for(int c: classes){
						String sql = "SELECT * FROM classes WHERE id=?";
						PreparedStatement ps = dbcon.prepareStatement(sql);
						ps.setInt(1, c);
						
						ResultSet rs = ps.executeQuery();
						
						if(rs.next()){
							String name = rs.getString(2);
							
							String start_time = rs.getString(3);
							Time start_time_t = Time.valueOf(start_time);

							StringTokenizer st = new StringTokenizer(start_time, ":");
							int start_h = Integer.parseInt(st.nextToken());
							int start_m = Integer.parseInt(st.nextToken());
							
							String end_time = rs.getString(4);
							Time end_time_t = Time.valueOf(end_time);
							
							st = new StringTokenizer(end_time, ":");
							int end_h = Integer.parseInt(st.nextToken());
							int end_m = Integer.parseInt(st.nextToken());
							
							System.out.println(" " + end_time_t + " " + start_time_t);
							
							boolean mon = rs.getBoolean(5);
							boolean tue = rs.getBoolean(6);
							boolean wed = rs.getBoolean(7);
							boolean thu = rs.getBoolean(8);
							boolean fri = rs.getBoolean(9);
							
							Random r = new Random();
							String a = "456789AB";
							String color = "#" + a.charAt(r.nextInt(a.length())) + a.charAt(r.nextInt(a.length())) + a.charAt(r.nextInt(a.length()));
							
							if(mon){
								out.println("{ title: '" + name + "', start: new Date(y,m,d-dd+" + 1 + "," + start_h + "," + start_m + "), " + 
												"end: new Date(y,m,d-dd+" + 1 + "," + end_h + "," + end_m + "), color: '" + color + "', allDay: false, editable: false },");
							}
							if(tue){
								out.println("{ title: '" + name + "', start: new Date(y,m,d-dd+" + 2 + "," + start_h + "," + start_m + "), " + 
												"end: new Date(y,m,d-dd+" + 2 + "," + end_h + "," + end_m + "), color: '" + color + "', allDay: false, editable: false },");
							}
							if(wed){
								out.println("{ title: '" + name + "', start: new Date(y,m,d-dd+" + 3 + "," + start_h + "," + start_m + "), " + 
												"end: new Date(y,m,d-dd+" + 3 + "," + end_h + "," + end_m + "), color: '" + color + "', allDay: false, editable: false },");
							}
							if(thu){
								out.println("{ title: '" + name + "', start: new Date(y,m,d-dd+" + 4 + "," + start_h + "," + start_m + "), " + 
												"end: new Date(y,m,d-dd+" + 4 + "," + end_h + "," + end_m + "), color: '" + color + "', allDay: false, editable: false },");
							}
							if(fri){
								out.println("{ title: '" + name + "', start: new Date(y,m,d-dd+" + 5 + "," + start_h + "," + start_m + "), " + 
												"end: new Date(y,m,d-dd+" + 5 + "," + end_h + "," + end_m + "), color: '" + color + "', allDay: false, editable: false },");
							}
							
							
						}
					}
					
				%>
			]
		});
		
	});

</script>
<style type='text/css'>

	body {
		margin-top: 40px;
		text-align: center;
		font-size: 12px;
		font-family: "Lucida Grande",Helvetica,Arial,Verdana,sans-serif;
		}

	#calendar {
		width: 850px;
		margin: 0 auto;
		}
	div.inline { float:left; }
	
</style>
</head>
<body>
<div class='inline' id='calendar'></div>
<div class='inline' id='tasks'>
	<%
		ArrayList<Integer> tasks = (ArrayList<Integer>) request.getSession().getAttribute("tasks");
		
		if(!tasks.isEmpty()){
			String task_list = "";
   			for(int task_id : tasks){
   				task_list = task_list + (", " + task_id);	
			}
   			task_list = task_list.substring(2);
   			String sql = "SELECT description, due_date FROM tasks WHERE id IN (" + task_list + ") AND is_completed=0 ORDER BY is_completed, due_date DESC";
			PreparedStatement ps = dbcon.prepareStatement(sql);
			ResultSet rs = ps.executeQuery();
			
			out.print("<div class='panel panel-default' style='position:relative; margin-top:75%; left:20px; background-color:#FFFFBB'>" +
							"<table id='todo_list' class='table' style='text-align:left; border-collapse:separate; border-spacing:5px'> <th> To-Do List </th>");
				
			while(rs.next()){
				out.print("<tr><td><div class='external-event'>" + rs.getString(1) + "</div></td></tr>");
			}
		
			out.print("</table></div>" + 
					"<div style='display:none'> <input type='checkbox' id='drop-remove' checked /> <label for='drop-remove'>remove after drop</label> </div> </div>");
		}

	%>
</div>
</body>
</html>
